#!/bin/bash

set -x

mkdir -p public
pelican content
cp *.png output/
cp *.png output/pages/
cp *.png output/category/
cp solverstack.svg output/
cp content/pages/*.png output/pages/
cp content/pages/*.gif output/pages/
cp -r output/* public/
