#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Theme
#THEME = '/home/florent/git/pelican-themes/MinimalXY'
THEME = './'

# Theme customizations
MINIMALXY_CUSTOM_CSS = 'static/custom.css'
MINIMALXY_FAVICON = 'favicon.ico'
MINIMALXY_START_YEAR = 2021
MINIMALXY_CURRENT_YEAR = 2021

AUTHOR = u'Solverstack Inria Bordeaux'
SITENAME = u'Solverstack'
#SITEURL = 'http://localhost:8000'
SITEURL = 'https://solverstack.gitlabpages.inria.fr'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
#FEED_ALL_ATOM = SITEURL
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Contact', 'mailto:distrib@inria.fr'),
#         ('GitLab', 'https://gitlab.inria.fr/solverstack'),
#         ('Inria', 'https://www.inria.fr'),)

DEFAULT_PAGINATION = 10
PAGE_ORDER_BY = 'attribute'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
