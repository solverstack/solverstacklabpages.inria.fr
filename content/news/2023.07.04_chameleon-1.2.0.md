Title: Chameleon 1.2.0
date: 2023-07-04 12:10
Category: News
author: Florent Pruvost

New release of Chameleon 1.2.0 is out !

 - [Changelog](https://gitlab.inria.fr/solverstack/chameleon/-/releases/v1.2.0)
 - [Download tarball](https://gitlab.inria.fr/api/v4/projects/616/packages/generic/source/v1.2.0/chameleon-1.2.0.tar.gz)
 - [Documentation](https://solverstack.gitlabpages.inria.fr/chameleon/)
 - [Debian/Ubuntu packages](https://gitlab.inria.fr/solverstack/chameleon/-/packages)
```sh
sudo apt-get update -y
wget https://gitlab.inria.fr/api/v4/projects/616/packages/generic/ubuntu_22.04/1.2.0/chameleon_1.2.0-1_amd64.deb
sudo apt-get install -y ./chameleon_1.2.0-1_amd64.deb
# to uninstall: sudo apt autoremove chameleon
```
 - [Brew package for MacOSX](https://gitlab.inria.fr/solverstack/brew-repo)
 - [Spack package for Linux/MacOSX](https://github.com/spack/spack)
 - [GNU Guix packages](https://gitlab.inria.fr/guix-hpc/guix-hpc) and [non-free channel](https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free)

Add the following in `~/.config/guix/channels.scm`
```
(cons (channel
        (name 'guix-hpc-non-free)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git"))
      %default-channels)
```

Install command
```sh
guix pull
guix install chameleon
```
