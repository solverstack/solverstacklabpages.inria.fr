Title: StarPU 1.4.4
date: 2024-02-02 14:00
Category: News
author: Nathalie Furmento

New release of StarPU 1.4.4 is out !

 - [Changelog](https://files.inria.fr/starpu/starpu-1.4.4/log.txt)
 - [Download](https://files.inria.fr/starpu/starpu-1.4.4/starpu-1.4.4.tar.gz)
