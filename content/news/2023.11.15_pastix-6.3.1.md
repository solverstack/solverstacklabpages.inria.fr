Title: PaStiX 6.3.1
date: 2023-11-15 18:00
Category: News
author: Florent Pruvost

New release of PaStiX 6.3.1 is out !

 - [Changelog](https://gitlab.inria.fr/solverstack/pastix/-/releases/v6.3.1)
 - [Download tarball](https://gitlab.inria.fr/solverstack/pastix//uploads/ad8f528a3f6c012762e9a903b28315f8/pastix-6.3.1.tar.gz)
 - [Documentation](https://solverstack.gitlabpages.inria.fr/pastix/index.html)
 - [Brew package for MacOSX](https://gitlab.inria.fr/solverstack/brew-repo)
 - [Spack package for Linux/MacOSX](https://github.com/spack/spack)
 - [GNU Guix packages](https://gitlab.inria.fr/guix-hpc/guix-hpc) and [non-free channel](https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free)

Add the following in `~/.config/guix/channels.scm`
```
(cons (channel
        (name 'guix-hpc-non-free)
        (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git"))
      %default-channels)
```

Install command
```sh
guix pull
guix install pastix
```
