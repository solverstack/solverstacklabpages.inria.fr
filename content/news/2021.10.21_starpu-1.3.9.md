Title: StarPU 1.3.9
date: 2021-10-21 17:00
Category: News
author: Nathalie Furmento

New release of StarPU 1.3.9 is out !

 - [Changelog](https://files.inria.fr/starpu/starpu-1.3.9/log.txt)
 - [Download](https://files.inria.fr/starpu/starpu-1.3.9/starpu-1.3.9.tar.gz)
