Title: FAQ
attribute: 3

# Table of Contents

1.  [Getting started with StarPU and Chameleon](#faq1)
2.  [Debugging StarPU and Chameleon within Guix](#faq2)

<a id="faq1"></a>

# Getting started with StarPU and Chameleon

* [StarPU](https://starpu.gitlabpages.inria.fr/): task-based runtime system
* [Chameleon](https://solverstack.gitlabpages.inria.fr/chameleon/):
  dense linear algebra library, built on top of StarPU.

Both are available as Guix (in the [Guix-HPC
channel](https://gitlab.inria.fr/guix-hpc/guix-hpc)) or Spack
packages.

## Building StarPU

```sh
sudo apt install libtool-bin libhwloc-dev libmkl-dev pkg-config
git clone https://gitlab.inria.fr/starpu/starpu.git
cd starpu
./autogen.sh
mkdir build
cd build
../configure --prefix=$HOME/dev/builds/starpu --disable-opencl --disable-cuda --disable-fortran
# see https://files.inria.fr/starpu/testing/master/doc/html/CompilationConfiguration.html
make -j20 install
```

Adjust environment variables (for example in your `.bash_profile`):
```sh
export PATH=$HOME/dev/builds/starpu/bin:${PATH}
export LD_LIBRARY_PATH=$HOME/dev/builds/starpu/lib/:${LD_LIBRARY_PATH}
export PKG_CONFIG_PATH=$HOME/dev/builds/starpu/lib/pkgconfig:${PKG_CONFIG_PATH}
```

After sourcing `.bash_profile`, you should be able to execute:
```sh
starpu_machine_display
```
Which shows which hardware is available on your local machine.

Full information on how to build StarPU is available [here](https://files.inria.fr/starpu/doc/html_web_installation/)


## Building Chameleon

```sh
sudo apt install cmake libmkl-dev
git clone --recurse-submodules https://gitlab.inria.fr/solverstack/chameleon.git
cd chameleon
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/dev/builds/chameleon/ -DCHAMELEON_USE_MPI=OFF -DCHAMELEON_ENABLE_EXAMPLE=OFF -DCHAMELEON_ENABLE_TESTING=ON -DCHAMELEON_PREC_C=OFF -DCHAMELEON_PREC_Z=OFF -DBLA_VENDOR=Intel10_64lp_seq
make -j20 install

$HOME/dev/builds/chameleon/bin/chameleon_stesting -o potrf -H # should print some nice results
```

## Distributed version

StarPU should have detected MPI during its building.

For Chameleon, you have to add the options `-DCHAMELEON_USE_MPI=ON
-DCHAMELEON_USE_MPI_DATATYPES=ON` to the `cmake` command line and build
again.

The common way of using distributed StarPU is to launch one MPI/StarPU
process per compute node, and then StarPU takes care of feeding all
available cores with task. You can run:

```sh
mpirun -np 4 $HOME/dev/builds/chameleon/bin/chameleon_stesting -o potrf -H
```

This will execute a Cholesky decomposition (`potrf`) with 4 MPI
processes (`-np 4`) and presents results in a human-readable way
(`-H`; for a CSV-like output, you can omit this option).

You can measure performance of different matrix size with the option
`-n 3200:32000:3200` (from matrix size 3200 to 32000 with a step of
3200).

You can do several iteration of the same matrix size with `--niter 2`.

## Basic performance tuning

A good matrix distribution is square 2D-block-cyclic, for this add `-P
x` where `x` should be (close to) the square root of the number of MPI
processes (ie, you should use a square number of compute nodes).

To get better results, you should bind the main thread:
```sh
export STARPU_MAIN_THREAD_BIND=1
```
Set the number of workers (CPU cores executing task) to the number of
cores available on the compute node minus one:
```sh
export STARPU_NCPU=15
```

You should not use hyperthreads.

To know what is the good matrix size range, just execute with sizes,
let's say, `3200:50000:3200`, plot the obtained Gflop/s and see with
which size you reach the plateau.

## Misc

Have a look on Section 3 about thread binding in [this presentation](https://ph-sw.fr/public/starpu-gt-1an-these.pdf)


<a id="faq2"></a>

# Debugging StarPU and Chameleon within Guix using GDB

## Get debug information for Chameleon (not StarPU because of a bug), without all chameleon sources

With Guix we should normally get degugging information of chameleon and starpu by adding the outputs "debug" to the guix shell command:
```sh
guix shell --pure chameleon chameleon:debug starpu:debug coreutils gdb xterm -- /bin/bash --norc
```

And debug info should be installed in `$GUIX_ENVIRONMENT/lib/debug`, thus we could add it to the GDB debug file directory as follows
```sh
echo "set debug separate-debug-file on" > gdbcommands
echo "set debug-file-directory $GUIX_ENVIRONMENT/lib/debug" >> gdbcommands
echo "start" >> gdbcommands
echo "b chameleon_starpu_init" >> gdbcommands
echo "b starpu_mpi_init_conf" >> gdbcommands
echo "continue" >> gdbcommands
```

And run with gdb
```sh
gdb -ex 'source gdbcommands' --args chameleon_dtesting -o potrf
```

Here you will be able to debug chameleon functions, see the stack, print variables.
Unfortunately, there is still difficulties to get debug info from starpu libraries because of a mismatch of paths from guix, *e.g.*
```
Looking for separate debug info (debug link) for /gnu/store/3qd11j3xpi0vfqhdj9hqwl8fd5n28czj-starpu-1.4.7/lib/libstarpumpi-1.4.so.3
  Trying /gnu/store/3qd11j3xpi0vfqhdj9hqwl8fd5n28czj-starpu-1.4.7/lib/libstarpumpi-1.4.so.3.0.1.debug... no, unable to open.
  Trying /gnu/store/3qd11j3xpi0vfqhdj9hqwl8fd5n28czj-starpu-1.4.7/lib/.debug/libstarpumpi-1.4.so.3.0.1.debug... no, unable to open.
  Trying /gnu/store/d5rd533nf3sn1hyq5ihxn6yw3liic6hk-profile/lib/debug//gnu/store/3qd11j3xpi0vfqhdj9hqwl8fd5n28czj-starpu-1.4.7/lib/libstarpumpi-1.4.so.3.0.1.debug... no, unable to open.
```

In addition you  dont' have access to the source code.
To add them you could tell gdb where are sources, for instance
```sh
export SOURCES_CHAMELEON=`guix build --source chameleon`
guix shell --pure --preserve=SOURCES chameleon chameleon:debug starpu:debug coreutils gdb xterm -- /bin/bash --norc

echo "set debug separate-debug-file on" > gdbcommands
echo "set debug-file-directory $GUIX_ENVIRONMENT/lib/debug" >> gdbcommands
echo "set substitute-path /tmp/guix-build-chameleon-1.2.0.drv-0/source $SOURCES_CHAMELEON" >> gdbcommands
echo "start" >> gdbcommands
echo "b chameleon_starpu_init" >> gdbcommands
echo "continue" >> gdbcommands

gdb -ex 'source gdbcommands' --args chameleon_dtesting -o potrf
```
But unfortunately many sources are missing, all ones generated in the build directory (which has been dropped by guix) which contain all algorithms related to a specific precision s, d, c, z.

Hence, in order to get all debugging and sources information we suggest to build by your own Chameleon and StarPU in a guix shell environment.

## Build StarPU and Chameleon by your own in a guix shell

Build starpu with guix adding debug symbols
```sh
cd /tmp
git clone https://gitlab.inria.fr/starpu/starpu.git
cd starpu
guix shell --pure -D starpu -- /bin/bash --norc
./autogen.sh
rm build -rf && mkdir build && cd build
../configure --prefix=$PWD/install --enable-debug --disable-opencl
make -j20 install
exit
```

Build chameleon with guix adding debug symbols
```sh
cd /tmp
git clone --recursive https://gitlab.inria.fr/solverstack/chameleon.git
cd chameleon
guix shell --pure -D chameleon -- /bin/bash --norc
export PKG_CONFIG_PATH=/tmp/starpu/build/install/lib/pkgconfig:$PKG_CONFIG_PATH
cmake -B build -DCMAKE_BUILD_TYPE=Debug -DCHAMELEON_USE_MPI=ON
cmake --build build -j20
exit
```

Debug Chameleon using GDB
```sh
cd /tmp
guix shell --pure -D chameleon gdb xterm -- /bin/bash --norc

# to execute successive gdb commands
echo "b starpu_mpi_init_conf" >> gdbcommands
echo "run" >> gdbcommands

# without mpi
gdb -ex 'source gdbcommands' --args /tmp/chameleon/build/testing/chameleon_stesting -o potrf

# with mpi
mpiexec --oversubscribe -n 2 xterm -hold -e gdb -ex 'source gdbcommands' --args /tmp/chameleon/build/testing/chameleon_stesting -o potrf -t 2
```

Example on Plafrim
```sh
 ssh -X plafrim

 # repeat the compilation steps of StarPU and Chameleon seen just above

 # reserve 2 nodes and to use 4 MPI processes (1 by socket)
 salloc --nodes=2 --time=01:00:00 --constraint bora --exclusive --ntasks-per-node=2 --threads-per-core=1

 # deploy the guix environment
 guix shell --pure --preserve='SLURM' -D chameleon gdb xterm slurm -- /bin/bash --norc

 # if openblas is used you should set number of threads to 1
 export OPENBLAS_NUM_THREADS=1

 # run with gdb: 4 xterm terminals will appear
 mpiexec --map-by socket xterm -hold -e gdb -ex run --args /tmp/chameleon/build/testing/chameleon_stesting -o potrf -n 32000 -P 2
```