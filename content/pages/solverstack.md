Title: Solverstack@Inria Bordeaux
URL:
save_as: index.html
attribute: 1

**Solverstack@Inria Bordeaux** aims at providing a high-performance (HPC) linear algebra solver stack. It provides a collection of solvers, partitioning tools and runtimes systems, which may be employed on modern supercomputers to operate on dense and sparse matrices.
It includes both direct and iterative methods (together with various preconditioners), as well as hybrid direct/iterative ones.

![Solverstack DAG](./solverstack.svg)

One of the main objective is the *portability of performance*, from the laptops to the supercomputer. The software are mainly developed in C, C++ and Fortran by researchers and engineers working in -- or in strong collababoration with -- [Inria](https://www.inria.fr/en) joint project-teams with [Bordeaux INP](https://www.bordeaux-inp.fr/en), [Bordeaux University](https://www.u-bordeaux.com/) and [CNRS](https://www.cnrs.fr/en) ([LaBRI UMR 5800](https://www.labri.fr/en)), [Concace](https://concace.gitlabpages.inria.fr/), [Topal](https://topal.gitlabpages.inria.fr/), [Storm](https://team.inria.fr/storm/) and [TADaaM](https://team.inria.fr/tadaam/) located at [Inria Bordeaux Sud-Ouest](https://www.inria.fr/en/centre-bordeaux-sud-ouest).

**Solverstack** is a collection of software libraries, with some dependencies between them.
This software collection provides:

 - a high performance communication library with [NewMadeleine](https://pm2.gitlabpages.inria.fr/),
 - a task-based support with the [StarPU](https://starpu.gitlabpages.inria.fr/) runtime system,
 - graphs partitioning and sparse matrix ordering with [Scotch](https://gitlab.inria.fr/scotch/scotch),
 - sparse direct methods in [PaStiX](https://solverstack.gitlabpages.inria.fr/pastix/) and [qr_mumps](http://buttari.perso.enseeiht.fr/qr_mumps/),
 - Krylov subspace iterative methods in [fabulous](https://solverstack.gitlabpages.inria.fr/fabulous/),
 - hybrid direct/iterative methods in [Composyx](https://gitlab.inria.fr/composyx/composyx),
 - dense direct methods in [Chameleon](https://solverstack.gitlabpages.inria.fr/chameleon/),
 - dense matrix randomized eigen and singular value decomposition methods in [Fmr](https://gitlab.inria.fr/compose/legacystack/fmr),
 - multivariate analysis (MVA) methods in [Cppdiodon](https://gitlab.inria.fr/diodon/cppdiodon).

See [libraries features](https://solverstack.gitlabpages.inria.fr/pages/solvers.html).

Check the [news](https://solverstack.gitlabpages.inria.fr/category/news.html) to get latest releases.

**Solverstack** is packaged with care for the high-performance in
[guix-hpc](https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free/),
[spack](https://spack.io/),
[brew](https://gitlab.inria.fr/solverstack/brew-repo) package managers.

**Contact:** [solverstack@inria.fr](mailto:solverstack@inria.fr)
